# civo-cluster

## Initialize

- You need to create a secret CI variable with your Civo API key => Add your API key to a CI variable: `CIVO_API_KEY`

## Sizes
```
+----------------+-------------+------------+-----+----------+-----------+------------+
| Name           | Description | Type       | CPU | RAM (MB) | Disk (GB) | Selectable |
+----------------+-------------+------------+-----+----------+-----------+------------+
| g3.k3s.xsmall  | Extra Small | Kubernetes |   1 |     1024 |        25 | Yes        |
| g3.k3s.small   | Small       | Kubernetes |   1 |     2048 |        25 | Yes        |
| g3.k3s.medium  | Medium      | Kubernetes |   2 |     4096 |        25 | Yes        |
| g3.k3s.large   | Large       | Kubernetes |   4 |     8192 |        25 | Yes        |
| g3.k3s.xlarge  | Extra Large | Kubernetes |   6 |    16384 |        25 | Yes        |
| g3.k3s.2xlarge | 2X Large    | Kubernetes |   8 |    32768 |        10 | Yes        |
+----------------+-------------+------------+-----+----------+-----------+------------+
```

## Setup of the kube integration on GitLab.com (or any GitLab instance)

- Ref: https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html

### Pre-Requisites

GitLab authenticates against Kubernetes using service tokens, which are scoped to a particular namespace. The token used should belong to a service account with cluster-admin privileges. To create this service account, create a file `gitlab-admin-service-account.yaml` with this content:

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: gitlab
    namespace: kube-system
```

And then:

```bash
kubectl apply -f gitlab-admin-service-account.yaml
```

### Steps

We'll connect the K3S cluster at the group level (on GitLab.com), but you can do it at the project level.

- go to clusters menu
  - click on <kbd>Integrate with a cluster certificate</kbd>
  - select the <kbd>Connect existing cluster</kbd> tab
  - fill the form:
    - **Kubernetes cluster name**: `k3s-mono-cluster` *(you can type what you want of course)*
    - **API URL**: `https://212.2.242.147:6443` *(you can find it in the `config/k3s.yaml` file at the `server` section)*
    - **CA Certificate**: copy/paste the content of `config/ca.txt`
    - **Service Token**: copy/paste the content of `config/token.txt`
  - click on <kbd>Add Kubernetes cluster</kbd>
  - on the next form (*Details*) fill **Base domain** with `212.2.242.147.nip.io` (*the cluster IP + `.nip.io`, `.xip.io` should work too*)
  - click on <kbd>Save changes</kbd>

### Add Prometheus and the Kubernetes executor

- Select the <kbd>Applications</kbd> tab
  - click on <kbd>Install</kbd> for **Prometheus**
  - click on <kbd>Install</kbd> for **GitLab Runner**
  - wait for a moment ⏳
- That's all 🎉
