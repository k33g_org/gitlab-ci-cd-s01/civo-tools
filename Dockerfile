FROM alpine:latest
#ENV KUBECTL_DISTRIBUTION="arm64"
ENV KUBECTL_DISTRIBUTION="amd64"
RUN apk --no-cache add curl \
    && curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/${KUBECTL_DISTRIBUTION}/kubectl \
    && chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl \
    && curl -sL https://civo.com/get | sh
CMD ["/bin/sh"]
